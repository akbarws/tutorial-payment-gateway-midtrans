package com.akbar.paymentgateway.pojo;

import java.util.List;

public class TransactionRequest {
	
	List<TransactionProductRequest> productRequests;

	public List<TransactionProductRequest> getProductRequests() {
		return productRequests;
	}

	public void setProductRequests(List<TransactionProductRequest> productRequests) {
		this.productRequests = productRequests;
	}

	public TransactionRequest() {}
	
	public TransactionRequest(List<TransactionProductRequest> productRequests) {
		super();
		this.productRequests = productRequests;
	}
	
	
}
