package com.akbar.paymentgateway.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.akbar.paymentgateway.pojo.TransactionCompleteResponse;
import com.akbar.paymentgateway.pojo.TransactionRequest;
import com.akbar.paymentgateway.pojo.TransactionResponse;
import com.akbar.paymentgateway.service.TransactionService;

@RestController
public class TransactionController {

	@Autowired
	TransactionService transactionService;
	
	@PostMapping(value = "/transaction/add", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TransactionResponse> add(@RequestBody TransactionRequest transactionRequest) {
		return ResponseEntity.ok(transactionService.add(transactionRequest));
	}
	
	@PostMapping(value = "/transaction/complete", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<TransactionCompleteResponse> complete(@RequestBody Map<String, Object> response) {
		return ResponseEntity.ok(transactionService.complete(response));
	}
	
}
