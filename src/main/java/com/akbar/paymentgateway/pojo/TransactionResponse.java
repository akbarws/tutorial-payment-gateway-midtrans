package com.akbar.paymentgateway.pojo;

import java.util.Date;
import java.util.List;

public class TransactionResponse {

	private Date date;
	private int status;
	List<TransactionDetailResponse> details;
	
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public List<TransactionDetailResponse> getDetails() {
		return details;
	}
	public void setDetails(List<TransactionDetailResponse> details) {
		this.details = details;
	}
	
	public TransactionResponse() {}
	
	public TransactionResponse(Date date, int status, List<TransactionDetailResponse> details) {
		super();
		this.date = date;
		this.status = status;
		this.details = details;
	}
	
	
}
