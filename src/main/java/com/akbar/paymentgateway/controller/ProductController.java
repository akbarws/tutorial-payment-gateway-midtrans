package com.akbar.paymentgateway.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.akbar.paymentgateway.pojo.ProductResponse;
import com.akbar.paymentgateway.service.ProductService;

@RestController
public class ProductController {
	
	@Autowired
	ProductService productService;
	
	@PostMapping(value = "/product/add", consumes = "application/json")
	public ResponseEntity<?> addProduct(@RequestBody ProductResponse productResponse) {
		return ResponseEntity.ok(productService.add(productResponse.getName(), productResponse.getPrice()));
	}

}
