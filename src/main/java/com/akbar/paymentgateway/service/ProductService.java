package com.akbar.paymentgateway.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.akbar.paymentgateway.entity.Product;
import com.akbar.paymentgateway.pojo.ProductResponse;
import com.akbar.paymentgateway.repository.ProductRepository;

@Service
public class ProductService {
	
	@Autowired
	ProductRepository productRepository;

	public ProductResponse add(String name, int price) {
		Product product = new Product(name, price);
		productRepository.save(product);
		return new ProductResponse(name, price);
	}
	
}
