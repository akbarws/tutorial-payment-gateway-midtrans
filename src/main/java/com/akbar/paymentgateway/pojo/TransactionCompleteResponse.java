package com.akbar.paymentgateway.pojo;

public class TransactionCompleteResponse {
	
	String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public TransactionCompleteResponse() {}

	public TransactionCompleteResponse(String message) {
		super();
		this.message = message;
	}

}
