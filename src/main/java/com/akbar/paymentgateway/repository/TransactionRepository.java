package com.akbar.paymentgateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.akbar.paymentgateway.entity.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

	@Query(value = "SELECT * FROM transaction WHERE payment_id = ?1", nativeQuery = true)
	Transaction getByPaymentId(String paymentId);

}
