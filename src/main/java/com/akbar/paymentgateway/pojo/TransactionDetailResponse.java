package com.akbar.paymentgateway.pojo;

public class TransactionDetailResponse {

	private String productName;
	private int qty;
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	
	public TransactionDetailResponse() {}
	
	public TransactionDetailResponse(String productName, int qty) {
		super();
		this.productName = productName;
		this.qty = qty;
	}
	
	
}
