package com.akbar.paymentgateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TutorialPaymentGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(TutorialPaymentGatewayApplication.class, args);
	}

}
