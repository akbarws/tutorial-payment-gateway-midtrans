package com.akbar.paymentgateway.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.akbar.paymentgateway.entity.Product;
import com.akbar.paymentgateway.entity.Transaction;
import com.akbar.paymentgateway.entity.Transactiondetail;
import com.akbar.paymentgateway.pojo.TransactionCompleteResponse;
import com.akbar.paymentgateway.pojo.TransactionDetailResponse;
import com.akbar.paymentgateway.pojo.TransactionProductRequest;
import com.akbar.paymentgateway.pojo.TransactionRequest;
import com.akbar.paymentgateway.pojo.TransactionResponse;
import com.akbar.paymentgateway.repository.ProductRepository;
import com.akbar.paymentgateway.repository.TransactionDetailRepository;
import com.akbar.paymentgateway.repository.TransactionRepository;
import com.midtrans.Config;
import com.midtrans.ConfigFactory;
import com.midtrans.httpclient.error.MidtransError;
import com.midtrans.service.MidtransCoreApi;
import com.midtrans.service.MidtransSnapApi;

@Service
public class TransactionService {

	@Autowired
	TransactionRepository transactionRepository;
	@Autowired
	TransactionDetailRepository transactionDetailRepository;
	@Autowired
	ProductRepository productRepository;
	
	public TransactionResponse add(TransactionRequest request) {
		Date trDate = new Date();
		String paymentId = UUID.randomUUID().toString();
		Transaction transaction = transactionRepository.save(new Transaction(paymentId, trDate, 0));
		int total = 0;
		List<TransactionDetailResponse> detailResponses = new ArrayList<TransactionDetailResponse>();
		for(TransactionProductRequest productRequest : request.getProductRequests()) {
			Product product = productRepository.getById(productRequest.getProductId());
			transactionDetailRepository.save(new Transactiondetail(transaction, product, productRequest.getQty()));
			total = total + product.getPrice() * productRequest.getQty();
			detailResponses.add(new TransactionDetailResponse(product.getName(), total));
		}
		String paymentToken = midTrans(paymentId, total);
		transaction.setPaymentToken(paymentToken);
		transactionRepository.save(transaction);
		return new TransactionResponse(trDate, 0, detailResponses);
	}
	
	public String midTrans(String paymentId, float total) {
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, String> transaction_details = new HashMap<String, String>();
		transaction_details.put("order_id", paymentId);
		transaction_details.put("gross_amount", String.valueOf(total));
		params.put("transaction_details", transaction_details);
		MidtransSnapApi snapApi = new ConfigFactory(
				new Config("SB-Mid-server-a2uRHUdt8fa6K53uhkllheYX", "SB-Mid-client-bDMBagI4d4VRLIzl", false)).getSnapApi();
		try {
			return snapApi.createTransactionToken(params);
		} catch (MidtransError e) {
			return e.getMessage();
		}
	}

	public TransactionCompleteResponse complete(Map<String, Object> response) {
		MidtransCoreApi coreApi = new ConfigFactory(
				new Config("SB-Mid-server-a2uRHUdt8fa6K53uhkllheYX", "SB-Mid-client-bDMBagI4d4VRLIzl", false))
				.getCoreApi();
		String paymentId =  response.get("order_id").toString();
		TransactionCompleteResponse transactionCompleteResponse = new TransactionCompleteResponse();
		try {
			JSONObject transactionResult = coreApi.checkTransaction(paymentId);
			String transactionStatus = (String) transactionResult.get("transaction_status");
			switch (transactionStatus) {
				case "settlement":
						Transaction transaction = transactionRepository.getByPaymentId(paymentId);
						transaction.setStatus(1);
						transactionRepository.save(transaction);
						transactionCompleteResponse.setMessage("Success");
					break;
				case "cancel":
					transactionCompleteResponse.setMessage("Canceled");
				case "deny":
					transactionCompleteResponse.setMessage("Deny");
				case "expire":
					transactionCompleteResponse.setMessage("Expired");
				case "pending":				
					transactionCompleteResponse.setMessage("Pending");
				default:
					throw new IllegalStateException("Unexpected value: " + transactionStatus);
			}
		} catch (MidtransError e) {
			e.printStackTrace();
		}

		return transactionCompleteResponse;
	}
}
