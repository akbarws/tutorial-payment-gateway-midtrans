package com.akbar.paymentgateway.pojo;

public class TransactionProductRequest {

	private int productId;
	private int qty;
	
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}
	public TransactionProductRequest(int productId, int qty) {
		super();
		this.productId = productId;
		this.qty = qty;
	}
	
}
