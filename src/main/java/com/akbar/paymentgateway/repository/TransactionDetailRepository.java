package com.akbar.paymentgateway.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.akbar.paymentgateway.entity.Transactiondetail;

public interface TransactionDetailRepository extends JpaRepository<Transactiondetail, Integer> {

}
